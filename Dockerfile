FROM python:3.9-alpine

COPY anybadges.py bootstrap.sh requirements.txt ./
RUN python3 -m venv anybadges \
    && source anybadges/bin/activate \
    && python3 -m pip install --upgrade pip \
    && pip install -r requirements.txt

EXPOSE 5000
ENTRYPOINT ["./bootstrap.sh"]
