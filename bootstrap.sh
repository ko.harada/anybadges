#!/bin/sh

export FLASK_APP=./anybadges.py

source anybadges/bin/activate

flask run -h 0.0.0.0
