from flask import Flask, request, Response
from anybadge import Badge

app = Flask(__name__)

@app.route('/', methods = ['POST'])
def create_badge():
    args = request.get_json()
    try:
        label = args.pop('label')
        value = args.pop('value')
        badge = Badge(label, value, **args)
        badge_image = badge.badge_svg_text
    except:
        return 'Keys "label" and "value" must be specified', 400

    return Response(badge_image, mimetype='image/svg+xml')
